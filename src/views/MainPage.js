import React, {useState} from 'react';
import News from '../components/News';
import './MainPage.css';

function MainPage() {

  return (
    <div className="MainPage">
        <h1>Центр детского и молодёжного инновационного творчества Технотроника</h1>
        <News />
    </div>
  );
}

export default MainPage;
