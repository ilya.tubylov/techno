import React, {useState} from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import MainPage from './views/MainPage';
import RoomPage from './views/RoomPage';
import ModalBox from './components/ModalBox';
import Login from './components/Login';
import Registration from './components/Registration';

function App() {

  const[modalBox, setModalBox] = useState(false)

  return (
    <div className="App">
      <Header setModalBox={setModalBox} />
      <Routes>
        <Route path='/' element={ <MainPage /> } />
        <Route path='/room' element={ <RoomPage /> } />
      </Routes>
      {
        modalBox
          ?
            <ModalBox setModalBox={setModalBox}>
              <Login />
              <Registration />
            </ModalBox>
          :
            null
      }
    </div>
  );
}

export default App;
