import React from 'react';
import { Link } from 'react-router-dom';
import './Nav.css';

function Nav() {
  return (
    <div className="Nav">
        <Link to='/' >Главная</Link>
    </div>
  );
}

export default Nav;
