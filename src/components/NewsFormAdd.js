import React from 'react';
import './NewsFormAdd.css';

function NewsFormAdd({setformNews}) {

  function newsAdd(){

    const header = document.getElementById('header').value
    const image = document.getElementById('image').value
    const content = document.getElementById('content').value

    const data = {
      header: header,
      image: image,
      content: content
    }

    const api = 'http://localhost:9001/newadd'

    fetch(api, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(result => result.json())
    .then((result) => {
      console.log( result)
    })
    
  }

  return (
    <div className="NewsFormAdd">
        <h1>Добавление новости</h1>
        <input id='header' type='text' placeholder='Введите заголовок новости...' />
        <input id='image' type='text' placeholder='Введите ссылку на изображение...' />
        <textarea id='content' rows={5} placeholder='Введите текст новости...'></textarea>
        <div className='NewsFormAdd__controls'>
            <button onClick={newsAdd}>Добавить</button>
            <button onClick={() => setformNews(false)}>Закрыть</button>
        </div>
    </div>
  );
}

export default NewsFormAdd;
