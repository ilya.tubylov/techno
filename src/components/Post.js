import React from 'react';
import './Post.css';

function Post({header, image, content}) {
  return (
    <div className="Post">
        <div className='Post__image'>
            <img src={image} />
        </div>
        <div className='Post__info'>
            <h2>{header}</h2>
            <p>{content}</p>
        </div>
        <div className='Post__delete'>X</div>
    </div>
  );
}

export default Post;
