import React, {useState} from 'react';
import Post from './Post';
import NewsFormAdd from './NewsFormAdd';
import './News.css';

function News() {

  const[formNews, setformNews] = useState(false)
  
  const header = 'Новость 1'
  const image = ''
  const content = 'Компью́тер (англ. computer, МФА: [kəmˈpjuː.tə(ɹ)][1] — «вычислитель», от лат. computare — считать, вычислять[2]) — термин, пришедший в русский язык из иноязычных (в основном — английских) источников, одно из названий электронной вычислительной машины.[3] Используется в данном смысле в русском литературном языке,[4][5] научной, научно-популярной литературе.[6]'


  const posts = [
    <Post key={1} header={header} image={image} content={content} />,
    <Post key={2} header={header} image={image} content={content} />,
    <Post key={3} header={header} image={image} content={content} />
  ]

  const api = 'http://localhost:9001/news'

  fetch(api, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    },
  })
  .then(result => result.json())
  .then((result) => {
    console.log( result)
  })

  return (
    <div className="News">
        <div className='News__admin_controls'>
          <button onClick={() => setformNews(true)}>Добавить новость</button>
        </div>
        {
          formNews ? <NewsFormAdd setformNews={setformNews} /> : null
        }
        {posts}
    </div>
  );
}

export default News;
